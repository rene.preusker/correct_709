# correct_709

This is a tiny script, demonstrating the usage of a transmission correction look up 
table for OLCI's  band 11 at around 709 nm. 

## Installation
You need a recent (>= 3.7) python instalation, including
numpy, scipy , netCDF4, lxml, time , datetime and calendar.
pytest is good to have for the testings.
Unpack/download/clone  the package wherever you want.  
Run:  

```bash  
$ python basic_tests.py   

$ python functional_tests.py    

$ pytest functional_tests.py
```
   
and clear all issues.
 

## Usage 
```bash   
$ python /path/to/OL_L1B.SEN3  /path/to/OL_WL2.SEN3  Name_of_your_result.nc4
```   





## Physics behind 
see pdf 

