# Copyright (c) 2019
# Author(s):
#   Rene Preusker <rene.preusker@gmail.com>
# Basic import
import sys,os,inspect
import pytest
import numpy as np


@pytest.mark.filterwarnings('ignore::RuntimeWarning')  # depending on version, numpy creates som (harmless) runtime warnings 
def test_interpolator(): 
    # Four dimensions initialisation
    
    import interpolate_mc as intmc
    
    # initialize the interpolation
    lut = np.arange(2*3*4*5,dtype=np.float).reshape(2,3,4,5)
    ax1 = np.array([3.,7.])
    ax2 = np.array([1.,5.,10.])[::-1]
    ax3 = np.array([1.,2., 4.,12.])
    ax4 = np.array([0.,1., 3., 8.,10.])[::-1]
    axes=(ax1,ax2,ax3,ax4)
    pn=intmc.interpolate_n(lut,axes)
     
    # Doit
    position = np.array([[3.5,5.5,12.,8.8]])  # (x1,x2,x3,x4)
    test = pn.recall(position)
    assert pytest.approx(53.1-test, abs=1.e-4) == 0.
    
def test_lutfile_ok():
    import demo_709_wv_correction
    if (os.path.isfile(demo_709_wv_correction.LUTFILE)):
        print('Found:',demo_709_wv_correction.LUTFILE)
    else:
        print(demo_709_wv_correction.LUTFILE, ' not found. You may have a problem.')
    assert os.path.isfile(demo_709_wv_correction.LUTFILE)

def test_lut_interpolator():
    import demo_709_wv_correction
    ff = demo_709_wv_correction.gen_interpolator()
    #     nrad, twv, aot,  wsp, azd, oza, sza
    inp = 0.02,  15, 0.2, 12.4, 78., 23., 34.
    tra  = ff(np.array(inp))
    assert pytest.approx(tra -0.9724 , abs=1.e-4) == 0. 


if __name__ == '__main__':
    test_interpolator()
    test_lutfile_ok()
    test_lut_interpolator()
