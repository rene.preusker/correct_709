import sys

print('   Testing python version:',sys.version_info, end='  ')
if (sys.version_info <= (3, 7, 0)):
    print('failed ')
    print('Your python version seems to be old. Should work, but is not tested!')
else:
    print('ok')


print('   Testing import of needed modules: ')
needed_modules=('numpy','netCDF4','scipy','lxml','datetime','pytest')
for nm in needed_modules:
    try:
        print('    ',nm,end='  ')
        dum=__import__(nm)
        print('ok')
    except :
        print('failed')
        print('Module ',nm,' could not imported. You may have a problem!')
