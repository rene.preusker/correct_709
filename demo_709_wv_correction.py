# Copyright (c) 2019
# Author(s):
#   Rene Preusker <rene.preusker@gmail.com>
import sys, os, inspect
import time, datetime, calendar
from lxml import etree as ET
from netCDF4 import Dataset
import numpy as np
import scipy.interpolate as interp
import interpolate_mc as intmc

BASEDIR = os.path.dirname(os.path.abspath(inspect.stack()[0].filename))
LUTFILE = os.path.join(BASEDIR, 'Transmission_lut_709_2019-10-29_13:31.nc4')
IGNORE_MASKS = False


def check_if_files_exists(*arg):
    for ff in arg:
        if ff is None:
            continue
        if not os.path.exists(ff):
            print(ff,' not found, exiting!')
            sys.exit()


def cosd(inn):
    return np.cos(inn*np.pi/180.)
def sind(inn):
    return np.sin(inn*np.pi/180.)
def acosd(inn):
    return np.arccos(inn)*180./np.pi
def asind(inn):
    return np.arcsin(inn)*180./np.pi

def azi2azid(sa,va):
    return acosd(cosd(sa)*cosd(va)+sind(sa)*sind(va))


def get_manifest(s3):
    '''
    returns an xml object
    '''
    xfx = os.path.join(s3,'xfdumanifest.xml')
    if xfx is None:
        return None

    with open(xfx) as fp:
        xfu=ET.parse(fp)
    return xfu.getroot()

def get_startTime(ff,tupel=False):
    xfu = get_manifest(ff)
    inf= xfu.find('.//sentinel-safe:startTime',xfu.nsmap).text.split(' ')
    if tupel is True:
        return time.strptime(inf[0],'%Y-%m-%dT%H:%M:%S.%fZ')
    else:
        return inf
def get_stopTime(ff,tupel=False):
    xfu = get_manifest(ff)
    inf= xfu.find('.//sentinel-safe:stopTime',xfu.nsmap).text.split(' ')
    if tupel is True:
        return time.strptime(inf[0],'%Y-%m-%dT%H:%M:%S.%fZ')
    else:
        return inf
def get_stopTimestamp(ff):
    return calendar.timegm(get_stopTime(ff,tupel=True))

def get_startTimestamp(ff):
    return calendar.timegm(get_startTime(ff,tupel=True))

def get_dimension(ff):
    xfu = get_manifest(ff)
    return (int(xfu.find('.//sentinel3:rows',xfu.nsmap).text) ,
            int(xfu.find('.//sentinel3:columns',xfu.nsmap).text))

def get_orbitNumber(ff):
    xfu = get_manifest(ff)
    inf= xfu.find('.//sentinel-safe:orbitNumber',xfu.nsmap).text
    return int(inf)

def get_envelop(ff):
    xfu = get_manifest(ff)
    lst= xfu.find('.//gml:posList',xfu.nsmap).text.split(' ')
    lat=np.array([float(lst[2*i])   for i in range(len(lst)//2)])
    lon=np.array([float(lst[2*i+1]) for i in range(len(lst)//2)])
    return lon,lat

def get_altitude(ff,st=(1,1)):
    with Dataset('%s/geo_coordinates.nc'%ff) as ds:
        alt=ds.variables['altitude'][::st[0],::st[1]]*1.
    return alt

def interpolate_tie(inn,al,ac):
    npa=np.arange
    npl=np.linspace
    sh_in=inn.shape
    sh_ou=((sh_in[0]-1)*al+1,(sh_in[1]-1)*ac+1)
    ou=interp.RectBivariateSpline(
          npl(0.,sh_ou[0],sh_in[0])
        , npl(0.,sh_ou[1],sh_in[1])
        , inn, kx=1, ky=1)(npa(sh_ou[0])
                         , npa(sh_ou[1]))
    return ou

def get_pressure(ff,st=(1,1)):
    with Dataset('%s/tie_meteo.nc'%ff) as ds:
        lat=ds.variables['sea_level_pressure'][:]*1.
        ac=ds.ac_subsampling_factor
        al=ds.al_subsampling_factor
    return interpolate_tie(lat,al,ac)[::st[0],::st[1]]

def get_temperature(ff,st=(1,1)):
    with Dataset('%s/tie_meteo.nc'%ff) as ds:
        tmp=ds.variables['atmospheric_temperature_profile'][:,:,0].squeeze()*1.
        ac=ds.ac_subsampling_factor
        al=ds.al_subsampling_factor
    return interpolate_tie(tmp,al,ac)[::st[0],::st[1]]

def get_wind(ff,st=(1,1)):
    with Dataset('%s/tie_meteo.nc'%ff) as ds:
        wsp=(ds.variables['horizontal_wind'][:,:,:]**2).sum(axis=2)**0.5
        ac=ds.ac_subsampling_factor
        al=ds.al_subsampling_factor
    return interpolate_tie(wsp,al,ac)[::st[0],::st[1]]

def get_tcw(ff,st=(1,1)):
    with Dataset('%s/tie_meteo.nc'%ff) as ds:
        tcw=ds.variables['total_columnar_water_vapour'][:]*1
        ac=ds.ac_subsampling_factor
        al=ds.al_subsampling_factor
    return interpolate_tie(tcw,al,ac)[::st[0],::st[1]]

def get_lsmask(s3,st=(1,1)):
    bit = flagname_to_flagbit(s3, datatype='l1')['land']
    with Dataset('%s/qualityFlags.nc'%s3) as ds:
        lsm=(ds.variables['quality_flags'][:,:] & 2**bit )== 2**bit
    return lsm[::st[0],::st[1]]

def get_latitude(ff,st=(1,1)):
    ## assumes FR dataset
    with Dataset('%s/tie_geo_coordinates.nc'%ff) as ds:
        lat=ds.variables['latitude'][:]*1.
        ac=ds.ac_subsampling_factor
        al=ds.al_subsampling_factor
    return interpolate_tie(lat,al,ac)[::st[0],::st[1]]

def get_longitude(ff,st=(1,1)):
    ## assumes FR dataset
    with Dataset('%s/tie_geo_coordinates.nc'%ff) as ds:
        lon=ds.variables['longitude'][:]*1.
        ac=ds.ac_subsampling_factor
        al=ds.al_subsampling_factor
    re=np.sin(lon*np.pi/180.)
    im=np.cos(lon*np.pi/180.)
    re_out=interpolate_tie(re,al,ac)[::st[0],::st[1]]
    im_out=interpolate_tie(im,al,ac)[::st[0],::st[1]]
    return np.arctan2(re_out,im_out)*180./np.pi

def get_geometry(ff,st=(1,1)):
    out={}
    with Dataset('%s/tie_geometries.nc'%ff) as ds:
        ac=ds.ac_subsampling_factor
        al=ds.al_subsampling_factor
        out={k: ds.variables[k][:]*1. for k in ds.variables}
    for k in out:
        out[k]=interpolate_tie(out[k],al,ac)[::st[0],::st[1]]
    out['ADA']=azi2azid(out['SAA'],out['OAA'])    
    return out

def get_cwl(ff,bn,st=(1,1)):
    with Dataset('%s/instrument_data.nc'%ff) as ds:
        l0=(ds.variables['lambda0'][bn-1,:]*1.).squeeze()
        di=ds.variables['detector_index'][:]*1
    return l0[di][::st[0],::st[1]]  

def get_solar_flux(ff,bn,st=(1,1)):
    with Dataset('%s/instrument_data.nc'%ff) as ds:
        sf=(ds.variables['solar_flux'][bn-1,:]*1.).squeeze()
        di=ds.variables['detector_index'][:]*1
    return sf[di][::st[0],::st[1]]    

def get_band(ff,bn,st=(1,1)):
    variable='Oa%02i_radiance'%bn
    with Dataset('%s/%s.nc'%(ff,variable)) as ds:
        out=ds.variables[variable][:]*1.    
    return out[::st[0],::st[1]]


def flagname_to_flagbit(s3, datatype='ocean'):
    if datatype == 'ocean':
        variable='WQSF'
        fname='wqsf'
    elif datatype == 'land':
        variable='LQSF'
        fname='lqsf'
    elif datatype == 'l1':
        variable='quality_flags'
        fname='qualityFlags'
    else:
        return None
    with Dataset('%s/%s.nc'%(s3,fname)) as ds:
        meanings = ds.variables[variable].flag_meanings.split()
        masks = ds.variables[variable].flag_masks
    flgs = {n: int(b).bit_length()-1 for n,b in  zip(meanings,masks)}
    return flgs


def simple_coast(lm, b=5):
    '''
    shift in 8 directions ...
    '''
    ss = ( (0,b), (0,-b),  (b,0 ), (-b,0),
           (b,b), (-b,-b), (b,-b), (-b,b))
    cs = lm.copy()
    for s in ss:
        for i in (0,1):
            cs = cs | np.roll(lm,s[i],i)
    
    return cs & (~lm)

def get_l2_ocean_cloudmask(ff,st=(1,1)):
    variable='WQSF'
    fname='wqsf'
    bit1 = flagname_to_flagbit(ff, datatype='ocean')['CLOUD']
    bit2 = flagname_to_flagbit(ff, datatype='ocean')['CLOUD_AMBIGUOUS']
    bit3 = flagname_to_flagbit(ff, datatype='ocean')['CLOUD_MARGIN']
    with Dataset('%s/%s.nc'%(ff,fname)) as ds:
        flg=ds.variables[variable][:,:][::st[0],::st[1]]
        out=((flg & 2**bit1 )== 2**bit1) | ((flg & 2**bit2) == 2**bit2) | ((flg & 2**bit3 ) ==2**bit3)
    return out

def test_agreement_and_exit_if_not(oll1, oll2):
    
    same_start = (get_startTimestamp(oll1) == get_startTimestamp(oll2))
    same_end = (get_stopTimestamp(oll1) == get_stopTimestamp(oll2))
    if not ( same_start and same_end):
        print('times of L1 and L2 do not fit, exiting')
        print('Starts:', get_startTimestamp(oll1),  get_startTimestamp(oll2), same_start ) 
        print('Stops:', get_stopTimestamp(oll1),  get_stopTimestamp(oll2), same_end ) 
        sys.exit()

    l1_shape = get_dimension(oll1)
    l2_shape = get_dimension(oll2)
    same_shape = (l2_shape == l1_shape) 
    if not same_shape:
        print('shapes of L1 and L2 do not fit, exiting')
        print('l1:', l1_shape)
        print('l2:', l2_shape)
        sys.exit()

    l1_orbit = get_orbitNumber(oll1)
    l2_orbit = get_orbitNumber(oll2)
    if not (l1_orbit == l2_orbit):
        print('orbits of L1 and L2 do not fit, exiting')
        print('l1:', l1_orbit)
        print('l2:', l2_orbit)
        sys.exit()

    l1_lon, l1_lat = get_envelop(oll1)
    l2_lon, l2_lat = get_envelop(oll2)
    lon_ok = (l1_lon.size == l2_lon.size) and ( np.sum(np.abs(l1_lon - l2_lon) < 0.0001) == l1_lon.size)
    lat_ok = (l1_lat.size == l2_lat.size) and ( np.sum(np.abs(l1_lat - l2_lat) < 0.0001) == l1_lat.size)
    if not (lon_ok or lat_ok):
        print('Polygon envelops of L1 and L2 do not fit, exiting')
        sys.exit()



def get_relevant_l1l2_data(oll1, oll2, stride = (4,4)): 
    '''
    '''
    l1_lon = get_longitude(oll1,stride)
    l1_lat = get_latitude(oll1,stride)

    rad = {k:get_band(oll1,k,stride)/get_solar_flux(oll1,k,stride) for k in [11,]}
    geo = get_geometry(oll1,stride)
    lon = get_longitude(oll1,stride)
    lat = get_latitude(oll1,stride)
    lsm = get_lsmask(oll1,stride)
    
    sct = simple_coast(lsm,1)
    tem = get_temperature(oll1,stride)
    wsp = get_wind(oll1,stride)
    tcw = get_tcw(oll1,stride)
    cld = get_l2_ocean_cloudmask(oll2,stride)
    lam11 = get_cwl(oll1,11,stride)
    
    mask = rad[11].mask
    for xx in rad:
        mask = mask | rad[xx].mask
    
    ok = ~ (mask | cld | lsm)   

    return {'lon': l1_lon, 'lat': l1_lat, 'geo': geo, 
            'rad': rad, 'lam11': lam11, 
            'lsm': lsm, 'sct': sct, 'cld': cld,
            'wsp': wsp, 'tem': tem, 'tcw': tcw, 
            'msk': mask,'ok':ok,'nnn': mask.size ,
            }

def prepare_data_for_interpolation_slow(data, aot = 0.2): 
    #ref, wvc, aot, wsp, wvl, azi, vie, suz
    def inp_gen():
        for i in range(data['nnn']):            
            if data['ok'].flat[i]:
                inp = [data['rad'][11].flat[i], 
                       data['tcw'].flat[i],
                       aot,
                       data['wsp'].flat[i],
                       #data['lam11'].flat[i],
                       180.-data['geo']['ADA'].flat[i],
                       data['geo']['OZA'].flat[i],
                       data['geo']['SZA'].flat[i],
                       ]
                yield inp
    _gen = inp_gen()
    return np.array([inp for inp in _gen])

def prepare_data_for_interpolation(data, aot = 0.2, ignore = False): 
    ok =  data['ok']
    if ignore is True:
        ok = ok | True
    nn = ok.sum()
    out = np.zeros((7,nn))
    out[0] = data['rad'][11][ok].flat[:]
    out[1] = data['tcw'][ok].flat[:]
    out[2] = aot
    out[3] = data['wsp'][ok].flat[:]
    out[4] = 180.-data['geo']['ADA'][ok].flat[:]
    out[5] = data['geo']['OZA'][ok].flat[:]
    out[6] = data['geo']['SZA'][ok].flat[:]
    
    return out.T

    
def gen_interpolator():
    #removing wvl until its simulated with wvl
    lut = {}
    with Dataset(LUTFILE) as ds:
        for v in ds.variables:
            lut[v] =( ds.variables[v][:]+0).filled(np.nan)        
        #axes = tuple(lut[d] for d in ds.variables['translut'].dimensions )
        axes = tuple(lut[d] for d in ds.variables['translut'].dimensions if not 'wvl' in d )
    #squeze out wvl, but could come later
    luts = lut['translut'].squeeze()
    itps=intmc.interpolate_n(luts,axes)
    function = lambda x: itps.recall(x,clip=True).T
    return function

def reorder_data(tran,ok, ignore = False):    
    if ignore is True:
        out = tran+0 
        out.shape= ok.shape
    else:
        out = np.ones(ok.shape)
        out[ok] = tran
    return out

def write_to_ncdf(outname,dct):
    yy= dct['lat'].shape[0]   
    xx= dct['lat'].shape[1]   
    for key in dct:
        if dct[key].ndim != 2:
            print( key,'is not 2 dimensional')
            return None
        if dct[key].shape[0] != yy:
            print( "Dimension 0 of ",key ,'does not agree.')
            print( "is:", dct[key].shape[0],"should be ",yy,"(like lat)")
            return None
        if dct[key].shape[1] != xx:
            print( "Dimension 1 of ",key ,'does not agree.')
            print( "is:", dct[key].shape[1],"should be ",xx,"(like lat)")
            return None
        if dct[key].dtype == np.bool:
            dct[key] = dct[key].astype(np.int8)

    with Dataset(outname, 'w', format='NETCDF4') as nc:
        nc.filename = outname
        nc.history = datetime.datetime.now().strftime('created on %Y-%m-%d %H:%M:%S UTC')
        nc.Conventions = "CF-1.4"
        nc.metadata_version = "0.5"
        nc.createDimension('y',yy)
        nc.createDimension('x',xx)
        for key in dct:
            if dct[key].dtype in (np.int8, np.int16, np.int32): 
                nc_dum=nc.createVariable(key,dct[key].dtype,('y','x'),zlib=True)
            else:
                nc_dum=nc.createVariable(key,dct[key].dtype,('y','x'),zlib=True, 
                                         fill_value=np.nan)

            nc_dum[:]=dct[key]
            #nc_dum._FillValue=np.nan



def doit():
    if len(sys.argv) != 4:
        print('Usage: %s l1file l2file result'%sys.argv[0])
        sys.exit()

    IGNORE_MASKS = False
    
    # 1. get sysargv
    l1_name = sys.argv[1]
    l2_name = sys.argv[2]
    cor_709_name = sys.argv[3]
    check_if_files_exists(l1_name,l2_name)
    test_agreement_and_exit_if_not(l1_name,l2_name)
    print('L1 L2 seem to be ok')
    
    print('Creating interpolator') 
    trans_interpolator = gen_interpolator()
    
    
    print('Reading data') 
    #data = get_relevant_l1l2_data(l1_name, l2_name, stride = (4,4))
    data = get_relevant_l1l2_data(l1_name, l2_name, stride = (1,1))
    print('Organizing data') 
    data_int = prepare_data_for_interpolation(data,0.15,ignore=IGNORE_MASKS)
    print('Calc transmission')
    transmission_15 = trans_interpolator(data_int)#.squeeze()
    print('Change aot,tcwv,wsp, and recalc transmission')
    data_int[:,2] = 0.05
    transmission_05 = trans_interpolator(data_int)
    data_int[:,2] = 0.3
    transmission_30 = trans_interpolator(data_int)
    print('Reordering data')
    data['transmission_709_05'] = reorder_data(transmission_05, data['ok'],ignore=IGNORE_MASKS)
    data['transmission_709_15'] = reorder_data(transmission_15, data['ok'],ignore=IGNORE_MASKS)
    data['transmission_709_30'] = reorder_data(transmission_30, data['ok'],ignore=IGNORE_MASKS)
    data['transmission_709'] = ( data['transmission_709_05'] + 
                                 data['transmission_709_15'] +
                                 data['transmission_709_30']) /3.
    
    
    # Uncertainty estimates 
    # 1. vs AOT
    d1 = np.abs(data['transmission_709'] - data['transmission_709_30'])
    d2 = np.abs(data['transmission_709'] - data['transmission_709_05'])
    d2_transmission_709_aot = np.maximum(d1,d2)**2
    data_int[:,2] = 0.15
    # 2. vs water vapor
    data_int[:,1] *= 1.1   # 10 % uncertainty in tcwv    
    d2_transmission_709_twv = (data['transmission_709_15'] - 
                               reorder_data(trans_interpolator(data_int), data['ok'], ignore=IGNORE_MASKS))**2
    # 3. vs wind speed
    data_int[:,1] /= 1.1   #     
    data_int[:,3] += 5.   #     
    d2_transmission_709_wsp = (data['transmission_709_15'] -
                               reorder_data( trans_interpolator(data_int), data['ok'], ignore=IGNORE_MASKS))**2
   
   
    data['d_transmission_709'] =  np.sqrt(d2_transmission_709_aot +
                                          d2_transmission_709_twv +
                                          d2_transmission_709_wsp)

    data['d_transmission_709_aot'] =  np.sqrt(d2_transmission_709_aot)
    data['d_transmission_709_wsp'] =  np.sqrt(d2_transmission_709_wsp)
    data['d_transmission_709_twv'] =  np.sqrt(d2_transmission_709_twv)
    
    data['toa_nom_rad_709'] = data['rad'][11]/data['transmission_709']
    data['d_toa_nom_rad_709'] = data['toa_nom_rad_709']/data['transmission_709'] * data['d_transmission_709'] 
   
        
    print('Writing result')
    outfields = (('transmission_709',np.float32),
                 ('toa_nom_rad_709',np.float32),
                 ('d_transmission_709',np.float32),
                 ('d_transmission_709_twv',np.float32),
                 ('d_transmission_709_aot',np.float32),
                 ('d_transmission_709_wsp',np.float32),
                 ('d_toa_nom_rad_709',np.float32),
                 ('lon',np.float),
                 ('lat',np.float),
                 ('ok',np.byte),
                 ('wsp',np.float32),
                 ('tcw',np.float32),
                 )
    write_to_ncdf(cor_709_name,{k[0]: data[k[0]].astype(k[1]) for k in outfields})
    
    #from matplotlib import pyplot as pl
    #pl.figure()
    #pl.imshow(data['transmission_709'])
    #pl.colorbar()
    #pl.show()

if __name__ == '__main__': 
    doit()
    pass
 




